import { connect } from 'react-redux';
import { testAction } from '../actions';
import Home from '../components/Home/Home';

const mapDispatchToProps = dispatch => ({
  test: () => dispatch(testAction()),
});

const mapStateToProps = state => ({
  testR: state.testReducer,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);
