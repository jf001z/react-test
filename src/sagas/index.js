import { all, call } from 'redux-saga/effects';
import sagas from './allSagas';

export default function* appSaga() {
  const allSagas = sagas.map(saga => call(saga));
  yield all([...allSagas]);
}
