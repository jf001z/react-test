import { takeEvery, put } from 'redux-saga/effects';
import { TEST_ACTION, TEST_ACTION_RESPONSE } from '../../actions/index';

function* testCallback() {
  yield put({ type: TEST_ACTION_RESPONSE, payload: 'This is a test' });
}
export default function* testSaga() {
  yield takeEvery(TEST_ACTION, testCallback);
}
