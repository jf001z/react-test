import testSaga from './testSaga';
import auth from './auth';

export default [testSaga, ...auth];
