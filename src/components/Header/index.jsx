// @flow
import React from 'react';
import styled from 'styled-components';

type Props = {};

const StyledHeader = styled.div`
  padding: 1em;
  background-color: rgba(121, 82, 179, 0.9);
`;
// eslint-disable-next-line
const Header = (props: Props) => <StyledHeader>Header</StyledHeader>;

export default Header;
