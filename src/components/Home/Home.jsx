// @flow
import React from 'react';

type Props = {
  test: Function,
  testR: any,
};

const Home = (props: Props) => {
  const { test, testR } = props;
  return (
    <div>
      <button
        onClick={() => {
          test();
        }}
      >
        Click me
      </button>
      <div>{testR}</div>
    </div>
  );
};

export default Home;
