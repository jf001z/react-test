// @flow
import React from 'react';
import styled from 'styled-components';

type Props = {};
// eslint-disable-next-line
const StyledSidebar = styled.div`
  background-color: #e14eca;
  height: 100%;
`;

const Index = (props: Props) => <StyledSidebar>SideBar</StyledSidebar>;

export default Index;
