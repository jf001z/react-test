import { combineReducers } from 'redux';
import testReducer from './testReducer';
import auth from './auth';

const defaultReducer = combineReducers({
  testReducer,
  auth,
});

export default defaultReducer;
