import { combineReducers } from 'redux';
import { reducer as oldReducers } from 'redux-oidc';
import { routerReducer } from 'react-router-redux';
import { REACT_TEST_AUTH_AUTHENTICATED } from '../../actions';

export default combineReducers({
  '@react-test/auth': combineReducers({ routing: routerReducer, oidc: oldReducers }),
});
