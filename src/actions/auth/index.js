export const REACT_TEST_AUTH_AUTHENTICATE = '@react-test/REACT_TEST_AUTH_AUTHENTICATE';
export const REACT_TEST_AUTH_AUTHENTICATED = '@react-test/REACT_TEST_AUTH_AUTHENTICATED';
export const doAuthentication = () => ({
  type: REACT_TEST_AUTH_AUTHENTICATE,
});
