export const TEST_ACTION = 'TEST_ACTION';
export const TEST_ACTION_RESPONSE = 'TEST_ACTION_RESPONSE';
export * from './auth';
export const testAction = () => ({ type: TEST_ACTION });
