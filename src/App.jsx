// @flow
import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import styled from 'styled-components';
import Header from './components/Header';
import SideBar from './components/SideBar';
import Home from './containers/testContainer';
import Auth from './components/AuthTest';
import { REACT_TEST_AUTH, REACT_TEST_HOME, REACT_TEST_TEST } from './config/router';

type Props = {
  isReady: boolean,
};
const AppDiv = styled.div`
  height: 100%;
`;
const ContentContainer = styled.div`
  display: flex;
  flex-direction: column;
  word-break: break-all;
  font-family: 'Roboto', 'Open Sans', sans-serif;
  font-size: 16px;
  height: 100%;
`;
const FlexHeader = styled(Header)`
  flex: 0;
`;
const FlexContent = styled.div`
  flex: 1;
  height: 100%;
`;
const GridContainer = styled.div`
  display: grid;
  height: 100%;
  grid-template-columns: 12% auto;
  grid-template-rows: 100%;
`;
const FlexSideBar = styled(SideBar)`
  flex: 1;
  width: 20%;
  margin: 2em;
  padding: 2em;
  height: 100%;
`;
const FlexShowContainer = styled.div``;
const App = (props: Props) => {
  // eslint-disable-next-line
  const { isReady } = props;
  return (
    <AppDiv className="App">
      <BrowserRouter>
        <ContentContainer>
          <FlexHeader />
          <FlexContent>
            <GridContainer>
              <FlexSideBar />
              <FlexShowContainer>
                <Switch>
                  <Route path={REACT_TEST_HOME} component={Home} exact />
                  <Route path={REACT_TEST_TEST} component={Home} />
                  <Route path={REACT_TEST_AUTH} component={Auth} />
                </Switch>
              </FlexShowContainer>
            </GridContainer>
          </FlexContent>
        </ContentContainer>
      </BrowserRouter>
    </AppDiv>
  );
};
export default App;
